const initialState = {

    todos : []

};

const reducer = (state = initialState, action) => {

    switch (action.type) {

        case 'ADD_TASK':
            return {
                ...state,
                todos: [...state.todos, action.payload]
            };

        case 'DELETE_TASK':
            return {
                ...state,
                todos: state.todos.filter(todo => todo.id !== action.payload)
            };

        case 'EDIT_TASK':
            return {
                ...state,
                todos: state.todos.map(todo => {

                    if(todo.id === action.payload.id){

                        return action.payload;

                    }

                    return todo;

                })
            };

        default:
            return state;

    }

}

export default reducer;