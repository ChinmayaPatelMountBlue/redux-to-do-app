import './styles/App.css';
import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import { nanoid } from 'nanoid';

function App() {

  const Todos = () => {

    const dispatch = useDispatch();

    const todos = useSelector(state => state.todos);
    
    const [isEditing, setIsEditing] = useState("");

    const [editTodo,setEditTodo] = useState("");

    const handleClick = (id) => dispatch({

      type: 'DELETE_TASK',
      payload: id

    });

    if(!todos || !todos.length){

      return <h2>No To-Dos</h2>;  

    }

    function setEditing (id) {

      isEditing ? setIsEditing("") : setIsEditing(id) ;

    }

    const handleEdit = event => {

      setEditTodo(event.target.value);

    }

    const handleSave = (id) => {

      if(editTodo === ""){
        alert("Empty To-Do Task");
      }
      else{

        dispatch({

          type: "EDIT_TASK",
          payload: {

            label: editTodo,
            id: id

          }

        });

      }

      setIsEditing("");

    }

    return (
      <ol className="task-list">
        {todos.map(todo => {
            return <li
              key={todo.id}
            >
              {(isEditing === todo.id) ? <input type="text" value={editTodo} onChange={handleEdit} className="task-input" /> : todo.label }
              <button
                className="btn"
                onClick={(isEditing) ? () => { handleSave(todo.id)} : () => { setEditing(todo.id); setEditTodo(todo.label)}}
              >
                {isEditing ? "SAVE" : "EDIT" }
              </button>
              <button
                className="btn"
                onClick={() => handleClick(todo.id)}
              >
                DELETE
              </button>
            </li>
          })
        }
      </ol>
    );

  };

  const ToDoInput = () => {

    const dispatch = useDispatch();

    const [newToDo, setNewToDo] = useState("");

    const handleChange = event => setNewToDo(event.target.value); 

    const handleClick = () => {

      if(newToDo) {

        dispatch({

          type: "ADD_TASK",
          payload: {
    
            label: newToDo,
            id: nanoid()
    
          }
    
        });

      }
      else{

        alert("Empty To-Do Task");

      }

      setNewToDo("");

    }

    return (

      <div className="form-container">
        <input type="text" value={newToDo} onChange={handleChange} className="task-input" />
        <button onClick={handleClick} className="btn">ADD</button>
      </div>

    );

  };

  return (

    <div className="app">

      <div className="container">

        <div className="header-container">

          <h1>To - Do</h1>

          <Todos />

          <ToDoInput />

        </div>

      </div>

    </div>

  );
}

export default App;
